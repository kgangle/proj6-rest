# Laptop Service
import pymongo
import sys
from pymongo import MongoClient
from flask import Flask, request
from flask_restful import Resource, Api



database = MongoClient('db', 27017)
db = database.tododb

# Instantiate the app
app = Flask(__name__)
api = Api(app)

class Laptop(Resource):
#replace this with brevet times
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }
api.add_resource(Laptop, '/')


class List_All(Resource):
	#this Resource returns all times in the database, in a JSON format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}, {'close_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_All, '/listAll')	

class List_All_json(Resource):
	#this Resource returns all times in the database, in a JSON format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}, {'close_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_All_json, '/listAll/json')

class List_Open_Only(Resource):
	#this Resource returns only the Open Times, in a JSON format	

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_Open_Only, '/listOpenOnly')

class List_Open_Only_json(Resource):
	#this Resource returns only the Open Times, in a JSON format	

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_Open_Only_json, '/listOpenOnly/json')

class List_Close_Only(Resource):
	#this Resource returns only the Close Times, in a JSON format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'close_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_Close_Only, '/listCloseOnly')

class List_Close_Only_json(Resource):
	#this Resource returns only the Close Times, in a JSON format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'close_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_Close_Only_json, '/listCloseOnly/json')

class List_All_csv(Resource):
	#This Resource returns all of the times in the database, in a csv format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}, {'close_times',pymongo.ASCENDING}).limit(limit)
		return_string = ''

		for date in dates:
			return_string += date['open_times'] + ", "
			return_string += date['close_times'] + ", "
		return return_string

api.add_resource(List_All_csv, '/listAll/csv')

class List_Close_csv(Resource):
	#This Resource returns only the Close Times, in a csv format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'close_times',pymongo.ASCENDING}).limit(limit)
		return_string = ''

		for date in dates:
			return_string += date['close_times'] + ", "
		return return_string

api.add_resource(List_Close_csv, '/listCloseOnly/csv')

class List_Open_csv(Resource):
	#This Resource returns only the Open Times, in a csv format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}).limit(limit)
		return_string = ''

		for date in dates:
			return_string += date['open_times'] + ", "
			return_string += date['close_times'] + ", "
		return return_string

api.add_resource(List_Open_csv, '/listOpenOnly/csv')





# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
