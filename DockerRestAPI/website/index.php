<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>List of Brevet Times</h1>
        <ul>
            <?php
            $List_All = file_get_contents('http://laptop-service/list_All');
           
			echo "All Times in database: \n";
            foreach ($List_All as $l) {
                echo "<li>$l</li>";
            }
			
			$List_All_json = file_get_contents('http://laptop-service/list_All/json');
           
			echo "All Times in database(JSON):  \n";
            foreach ($List_All_json as $l) {
                echo "<li>$l</li>";
            }

			$List_Open_Only = file_get_contents('http://laptop-service/list_Open_Only');
           
			echo "All Open Times in database:  \n";
            foreach ($List_Open_Only as $l) {
                echo "<li>$l</li>";
            }
		
			$List_Open_Only_json = file_get_contents('http://laptop-service/list_Open_Only/json');
           
			echo "All Open Times in database(JSON):  \n";
            foreach ($List_Open_Only_json as $l) {
                echo "<li>$l</li>";
            }

			$List_Close_Only = file_get_contents('http://laptop-service/list_Close_Only');
           
			echo "All Close Times in database:  \n";
            foreach ($List_Close_Only as $l) {
                echo "<li>$l</li>";
            }

			$List_Close_Only_json = file_get_contents('http://laptop-service/list_Close_Only/json');
           
			echo "All Close Times in database(JSON):  \n";
            foreach ($List_Close_Only_json as $l) {
                echo "<li>$l</li>";
            }

			$List_All_csv = file_get_contents('http://laptop-service/list_All_csv');
           
			echo "All Times in database (csv): \n";
            foreach ($List_All_csv as $l) {
                echo "<li>$l</li>";
            }

			$List_Close_csv = file_get_contents('http://laptop-service/list_Close_csv');
           
			echo "All Times in database (csv): \n ";
            foreach ($List_Close_csv as $l) {
                echo "<li>$l</li>";
            }

			$List_Open_csv = file_get_contents('http://laptop-service/list_Open_csv');
           
			echo "All Open Times in database (csv):  \n";
            foreach ($List_Open_csv as $l) {
                echo "<li>$l</li>";
            }


            ?>




        </ul>
    </body>
</html>
